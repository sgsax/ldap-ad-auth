# ldap-ad-auth

This is a set of configs that will allow you to authenticate your Linux machine against a Windows Active Directory using LDAP and Kerberos. These are sanitized working versions of the configs I am actively using on my systems. Of course, you should make changes where appropriate, or if something else works better for you. These are intended to be a starting point for getting authentication working on your own systems.

For Ubuntu and Debian variants you will need to install the following packages:
 * libnss-ldap
 * libpam-krb5
 * libpam-ldap
 * krb5-user
 * ldap-utils

For Redhat variants, you will need to install the following packages:
 * nss-pam-ldapd
 * pam_krb5
 * krb5-workstation

Look in the directory for your particular distro to see the file structure on your target host. Update hostname, domain, and LDAP bind account information as appropriate in each file.

## Point of order
To be clear, the method outlined here does not add the linux host to the AD domain. It will allow you to login to the linux host using credentials from the AD domain. It replaces looking up user info in /etc/passwd and /etc/group with looking it up in LDAP against the AD domain.

In my environment, we have added homedir, uid, and gid to the user object in AD, and have network file systems mounted at each linux host, so the user homedir and file permissions follow them no matter what linux host they login to.

If you have samba tools installed on the linux host, you can use "net join" to add it to the domain, and you will subsequently find it when browsing or searching the domain, but doing so has no real meaning. The black magic that the AD performs on joined computers does not apply to linux hosts. You can't apply GPOs to a linux host, or run login scripts from your domain.
